import logging
import random
import time

from discord import Colour, Embed
from discord.ext.commands import (
    BadArgument, Bot,
    Context, Converter, command
)

from bot.constants import (
    Channels, Cooldowns, ERROR_REPLIES, Keys, Roles, URLs
)
from bot.decorators import with_role
from bot.pagination import LinePaginator


log = logging.getLogger(__name__)

TEST_CHANNELS = (
    Channels.devtest,
    Channels.bot,
    Channels.helpers
)


class TagNameConverter(Converter):
    @staticmethod
    async def convert(ctx: Context, tag_name: str):
        def is_number(value):
            try:
                float(value)
            except ValueError:
                return False
            return True

        tag_name = tag_name.lower().strip()

        # The tag name has at least one invalid character.
        if ascii(tag_name)[1:-1] != tag_name:
            log.warning(f"{ctx.author} tried to put an invalid character in a tag name. "
                        "Rejecting the request.")
            raise BadArgument("Don't be ridiculous, you can't use that character!")

        # The tag name is either empty, or consists of nothing but whitespace.
        elif not tag_name:
            log.warning(f"{ctx.author} tried to create a tag with a name consisting only of whitespace. "
                        "Rejecting the request.")
            raise BadArgument("Tag names should not be empty, or filled with whitespace.")

        # The tag name is a number of some kind, we don't allow that.
        elif is_number(tag_name):
            log.warning(f"{ctx.author} tried to create a tag with a digit as its name. "
                        "Rejecting the request.")
            raise BadArgument("Tag names can't be numbers.")

        # The tag name is longer than 127 characters.
        elif len(tag_name) > 127:
            log.warning(f"{ctx.author} tried to request a tag name with over 127 characters. "
                        "Rejecting the request.")
            raise BadArgument("Are you insane? That's way too long!")

        return tag_name


class TagContentConverter(Converter):
    @staticmethod
    async def convert(ctx: Context, tag_content: str):
        tag_content = tag_content.strip()

        # The tag contents should not be empty, or filled with whitespace.
        if not tag_content:
            log.warning(f"{ctx.author} tried to create a tag containing only whitespace. "
                        "Rejecting the request.")
            raise BadArgument("Tag contents should not be empty, or filled with whitespace.")

        return tag_content


class Tags:
    """
    Save new tags and fetch existing tags.
    """

    def __init__(self, bot: Bot):
        self.bot = bot
        self.tag_cooldowns = {}
        self.headers = {"X-API-KEY": Keys.site_api}

    async def get_tag_data(self, tag_name=None, aliases_of=None) -> dict:
        """
        Retrieve the tag_data from our API

        :param tag_name: the tag to retrieve
        :return:
        if tag_name was provided, returns a dict with tag data.
        if not, returns a list of dicts with all tag data.

        """
        params = {}

        if tag_name:
            params["tag_name"] = tag_name
        elif aliases_of:
            params["aliases_of"] = aliases_of

        response = await self.bot.http_session.get(URLs.site_tags_api, headers=self.headers, params=params)
        tag_data = await response.json()

        return tag_data

    async def post_tag_data(self, tag_name: str, tag_content: str) -> dict:
        """
        Send some tag_data to our API to have it saved in the database.

        :param tag_name: The name of the tag to create or edit.
        :param tag_content: The content of the tag.
        :return: json response from the API in the following format:
        {
            'success': bool
        }
        """

        params = {
            'tag_name': tag_name,
            'tag_content': tag_content
        }

        response = await self.bot.http_session.post(URLs.site_tags_api, headers=self.headers, json=params)
        tag_data = await response.json()

        return tag_data

    async def patch_tag_data(self, tag_name: str, tag_alias: str) -> dict:
        """
        Send some tag_data to our API to have it saved in the database.

        :param tag_name: The name of the tag to be aliased.
        :param tag_alias: The new alias for the tag.
        :return: json response from the API in the following format:
        {
            'success': bool
        }
        """

        params = {
            'tag_name': tag_name,
            'tag_alias': tag_alias
        }

        response = await self.bot.http_session.patch(URLs.site_tags_api, headers=self.headers, json=params)
        tag_data = await response.json()

        return tag_data

    async def delete_tag_data(self, tag_name: str = None, tag_alias: str = None) -> dict:
        """
        Delete a tag using our API.

        :param tag_name: The name of the tag to delete.
        :param tag_alias: The name of the alias to delete.
        :return: json response from the API in the following format:
        {
            'success': bool
        }
        """

        params = {}

        if tag_name:
            params['tag_name'] = tag_name
        elif tag_alias:
            params['tag_alias'] = tag_alias

        response = await self.bot.http_session.delete(URLs.site_tags_api, headers=self.headers, json=params)
        tag_data = await response.json()

        return tag_data

    @command(name="tags()", aliases=["tags"], hidden=True)
    async def info_command(self, ctx: Context):
        """
        Show available methods for this class.

        :param ctx: Discord message context
        """

        log.debug(f"{ctx.author} requested info about the tags cog")
        return await ctx.invoke(self.bot.get_command("help"), "Tags")

    @command(name="tags.get()", aliases=["tags.get", "tags.show()", "tags.show", "get_tag"])
    async def get_command(self, ctx: Context, tag_name: TagNameConverter=None):
        """
        Get a list of all tags or a specified tag.

        :param ctx: Discord message context
        :param tag_name:
        If provided, this function shows data for that specific tag.
        If not provided, this function shows the caller a list of all tags.
        """

        def _command_on_cooldown(tag_name) -> bool:
            """
            Check if the command is currently on cooldown.
            The cooldown duration is set in constants.py.

            This works on a per-tag, per-channel basis.
            :param tag_name: The name of the command to check.
            :return: True if the command is cooling down. Otherwise False.
            """

            now = time.time()

            cooldown_conditions = (
                tag_name
                and tag_name in self.tag_cooldowns
                and (now - self.tag_cooldowns[tag_name]["time"]) < Cooldowns.tags
                and self.tag_cooldowns[tag_name]["channel"] == ctx.channel.id
            )

            if cooldown_conditions:
                return True
            return False

        if _command_on_cooldown(tag_name):
            time_left = Cooldowns.tags - (time.time() - self.tag_cooldowns[tag_name]["time"])
            log.warning(f"{ctx.author} tried to get the '{tag_name}' tag, but the tag is on cooldown. "
                        f"Cooldown ends in {time_left:.1f} seconds.")
            return

        tags = []

        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.get_tag_data(tag_name)

        # If we found something, prepare that data
        if tag_data:
            embed.colour = Colour.blurple()

            if tag_name:
                log.debug(f"{ctx.author} requested the tag '{tag_name}'")
                embed.title = tag_name

                if ctx.channel.id not in TEST_CHANNELS:
                    self.tag_cooldowns[tag_name] = {
                        "time": time.time(),
                        "channel": ctx.channel.id
                    }

            else:
                embed.title = "**Current tags**"

            if isinstance(tag_data, list):
                log.debug(f"{ctx.author} requested a list of all tags")
                tags = [f"**»**   {tag['tag_name']}" for tag in tag_data]
                tags = sorted(tags)

            else:
                embed.description = tag_data['tag_content']

        # If not, prepare an error message.
        else:
            embed.colour = Colour.red()

            if isinstance(tag_data, dict):
                log.warning(f"{ctx.author} requested the tag '{tag_name}', but it could not be found.")
                embed.description = f"**{tag_name}** is an unknown tag name. Please check the spelling and try again."
            else:
                log.warning(f"{ctx.author} requested a list of all tags, but the tags database was empty!")
                embed.description = "**There are no tags in the database!**"

            if tag_name:
                embed.set_footer(text="To show a list of all tags, use bot.tags.get().")
                embed.title = "Tag not found."

        # Paginate if this is a list of all tags
        if tags:
            if ctx.invoked_with == "tags.keys()":
                detail_invocation = "bot.tags[<tagname>]"
            elif ctx.invoked_with == "tags.get()":
                detail_invocation = "bot.tags.get(<tagname>)"
            else:
                detail_invocation = "bot.tags.get <tagname>"

            log.debug(f"Returning a paginated list of all tags.")
            return await LinePaginator.paginate(
                (lines for lines in tags),
                ctx, embed,
                footer_text=f"To show a tag, type {detail_invocation}.",
                empty=False,
                max_lines=15
            )

        return await ctx.send(embed=embed)

    @with_role(Roles.admin, Roles.owner, Roles.moderator)
    @command(name="tags.set()", aliases=["tags.set", "tags.add", "tags.add()", "tags.edit", "tags.edit()", "add_tag"])
    async def set_command(self, ctx: Context, tag_name: TagNameConverter, tag_content: TagContentConverter):
        """
        Create a new tag or edit an existing one.

        :param ctx: discord message context
        :param tag_name: The name of the tag to create or edit.
        :param tag_content: The content of the tag.
        """

        tag_name = tag_name.lower().strip()
        tag_content = tag_content.strip()

        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.post_tag_data(tag_name, tag_content)

        if tag_data.get("success"):
            log.debug(f"{ctx.author} successfully added the following tag to our database: \n"
                      f"tag_name: {tag_name}\n"
                      f"tag_content: '{tag_content}'")
            embed.colour = Colour.blurple()
            embed.title = "Tag successfully added"
            embed.description = f"**{tag_name}** added to tag database."
        else:
            log.error("There was an unexpected database error when trying to add the following tag: \n"
                      f"tag_name: {tag_name}\n"
                      f"tag_content: '{tag_content}'\n"
                      f"response: {tag_data}")
            embed.title = "Database error"
            embed.description = ("There was a problem adding the data to the tags database. "
                                 "Please try again. If the problem persists, see the error logs.")

        return await ctx.send(embed=embed)

    @with_role(Roles.admin, Roles.owner)
    @command(name="tags.delete()", aliases=["tags.delete", "tags.remove", "tags.remove()", "remove_tag"])
    async def delete_command(self, ctx: Context, tag_name: TagNameConverter):
        """
        Remove a tag from the database.

        :param ctx: discord message context
        :param tag_name: The name of the tag to delete.
        """

        tag_name = tag_name.lower().strip()
        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.delete_tag_data(tag_name=tag_name)

        if tag_data.get("success"):
            log.debug(f"{ctx.author} successfully deleted the tag called '{tag_name}'")
            embed.colour = Colour.blurple()
            embed.title = 'Tag successfully removed.'
            embed.description = f"**{tag_name}** removed from the tag database."

        else:
            log.debug(f"{ctx.author} tried to delete a tag called '{tag_name}', but the tag does not exist.")
            embed.colour = Colour.red()
            embed.title = "Tag not found."
            embed.description = f"Tag **{tag_name}** doesn't appear to exist."

        return await ctx.send(embed=embed)

    @with_role(Roles.admin, Roles.owner)
    @command(name="tags.set_alias()", aliases=["tags.set_alias", "tags.add_alias"])
    async def set_alias(self, ctx: Context, tag_name: TagNameConverter, tag_alias: TagNameConverter):
        """
        Create a new alias or edit an existing one.
        :param ctx: discord message context
        :param tag_name: tag name whose going to be aliased
        :param tag_alias: the alias to be used
        """

        tag_name = tag_name.lower().strip()
        tag_alias = tag_alias.lower().strip()

        embed = Embed()
        embed.colour = Colour.red()
        tag_exist = await self.get_tag_data(tag_name)

        if tag_exist:
            tag_data = await self.patch_tag_data(tag_name, tag_alias)

            if tag_data.get("success"):
                log.debug(f"{ctx.author} successfully added the following alias to our database: \n"
                          f"tag_name: {tag_name}\n"
                          f"tag_alias: '{tag_alias}'")
                embed.colour = Colour.blurple()
                embed.title = "Alias successfully added"
                embed.description = f"**{tag_alias}** added to tag database."
            else:
                log.error("There was an unexpected database error when trying to add the following alias: \n"
                          f"tag_name: {tag_name}\n"
                          f"tag_alias: '{tag_alias}'\n"
                          f"response: {tag_data}")
                embed.title = "Database error"
                embed.description = ("There was a problem adding the data to the tags database. "
                                     "Please try again. If the problem persists, see the error logs.")
        else:
            log.error(
                f"Tag {tag_name} doesn't exist. Couldn't add aliases for it.\n")
            embed.title = "Tag not found."
            embed.description = f"**{tag_name}** is an unknown tag name, couldn't add the alias."

        return await ctx.send(embed=embed)

    @with_role(Roles.admin, Roles.owner)
    @command(name="tags.delete_alias()", aliases=["tags.delete_alias", "tags.remove_alias"])
    async def delete_alias(self, ctx: Context, tag_alias: TagNameConverter):
        """
        Remove an alias from the database.

        :param ctx: discord message context
        :param tag_alias: The name of the alias to delete.
        """

        tag_alias = tag_alias.lower().strip()
        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.delete_tag_data(tag_alias=tag_alias)

        if tag_data.get("success"):
            log.debug(f"{ctx.author} successfully deleted the alias called '{tag_alias}'")
            embed.colour = Colour.blurple()
            embed.title = 'Alias successfully removed.'
            embed.description = f"**{tag_alias}** removed from the tag database."

        else:
            log.debug(f"{ctx.author} tried to delete an alias called '{tag_alias}', but the alias does not exist.")
            embed.colour = Colour.red()
            embed.title = "Alias doesn't exist."
            embed.description = f"Alias **{tag_alias}** doesn't appear to exist."

        return await ctx.send(embed=embed)

    @with_role(Roles.owner, Roles.admin)
    @command(name="tags.aliases_of()", aliases=["tags.aliases_of"])
    async def get_all_aliases(self, ctx: Context, tag_name: str):
        """

        :param ctx:
        :param tag_name:
        :return:
        """
        embed = Embed()
        embed.colour = Colour.red()
        tag_data = await self.get_tag_data(aliases_of=tag_name)

        if tag_data:
            tag_data = iter(tag_data)
            original_tag = next(tag_data)
            aliases = [f"**»**   {tag}" for tag in tag_data]
            if aliases:
                embed.colour = Colour.blurple()
                embed.title = f"Aliases of {original_tag}"
                return await LinePaginator.paginate(
                    (lines for lines in aliases),
                    ctx, embed,
                    footer_text=f"To show a tag, type bot.tags.get(tag_name).",
                    empty=False,
                    max_lines=15
                )
            else:
                embed.title = "Aliases not found."
                embed.description = f"There are no aliases for tag **{original_tag}**."
                embed.set_footer(text="To show a list of all tags, use bot.tags.get.")
                return await ctx.send(embed=embed)

        if isinstance(tag_data, list) and not tag_data:
            embed.title = "Tag not found."
            embed.description = f"No tag by the name **{tag_name}** could be found. Aliases could not be retrieved."
            embed.set_footer(text="To show a list of all tags, use bot.tags.get.")
            return await ctx.send(embed=embed)

    @get_command.error
    @set_command.error
    @delete_command.error
    async def command_error(self, ctx, error):
        if isinstance(error, BadArgument):
            embed = Embed()
            embed.colour = Colour.red()
            embed.description = str(error)
            embed.title = random.choice(ERROR_REPLIES)
            await ctx.send(embed=embed)
        else:
            log.error(f"Unhandled tag command error: {error} ({error.original})")

    @command(name="tags.keys()")
    async def keys_command(self, ctx: Context):
        """
        Alias for `tags.get()` with no arguments.

        :param ctx: discord message context
        """

        return await ctx.invoke(self.get_command)


def setup(bot):
    bot.add_cog(Tags(bot))
    log.info("Cog loaded: Tags")
